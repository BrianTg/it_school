#include <iostream>

#include "Person.h"
#include "BankAccount.h"

int main()
{
    // Do the necessary items in Person.h

    // Test default constructor
    Person p1("Mary", "8 St. Thomas Lane", 1979);
    //Test parametrised constructor
    Person p2("John", "8 St. Thomas Lane", 1979);
    
    // Test print
    std::cout << p1.print() << std::endl;
    std::cout << p2.print() << std::endl;

    //Test getName
    std::cout <<"Name: "<< p2.getName() << std::endl;

    // Do the necessary items in BankAccount.h

    BankAccount ba1(&p1);

    std::cout << "test ba1 has ballance: " << ba1.getBalance()<<std::endl;

    ba1.deposit(100.6);
    std::cout << "test ba1 has ballance: " << ba1.getBalance() << std::endl;

    BankAccount ba2(&p2);
    ba2.deposit(77.6);
    std::cout << "test ba2 has ballance: " << ba2.getBalance() << std::endl;

    

    if (ba2.withdraw(50.0) == !true)
    {
        std::cout << "Opperation was not successful "<< std::endl;
    }
    std::cout << "test ba2 has ballance: " << ba2.getBalance() << std::endl;
    
    if (!ba2.withdraw(50.0))
    {
        std::cout << "Opperation was not successful " << std::endl;
    }
    std::cout << "test ba2 has ballance: " << ba2.getBalance() << std::endl;

    std::cout << ba2.print() << std::endl;
    std::cout << ba2.printAll() << std::endl;

 
    // Do the necessary items in Bank.h
}