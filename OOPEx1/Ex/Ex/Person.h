#pragma once

#include <string>
// Create a class named Person. Implement all methods and test them.

// Each person has a name an address and a birth year
// Create a default constructor that builds a Person and sets its name and address to "unknown" and birt year to 0
// Create a method that returns a string like this:
//			Name: [Person name] Address: [Person address] Birth year: [Birth year]
// Create a parametrised constructor that builds a Person and takes a name address and birth year.
// Create a method that gets the name of a Person


class Person 
{
public:
	Person();
	Person(const std::string& newName, const std::string& newAddress, int newBirthYear);
	std::string print();
	std::string getName();

private:
	std::string name;
	std::string address;
	int birthYear;
};