#include "BankAccount.h"


BankAccount::BankAccount(Person* newOwner)
{
	this->owner = newOwner;
	this->balance = 0;
}

float BankAccount::getBalance()
{
	return this->balance;
}

void  BankAccount::deposit(float ammount)
{
	this->balance += ammount;
}

bool BankAccount::withdraw(float ammount)
{
	bool resultOfOpperation = false;
	if (this->balance < ammount)
	{
		resultOfOpperation = false;
	}
	else
	{
		resultOfOpperation = true;
		this->balance -= ammount;
	}
	return resultOfOpperation;
}

std::string BankAccount::print()
{
	std::string result = "";
	result = "Name: ";

	// this			- > BankAccount*
	// this->owner	- > Person*

	result += this->owner->getName();
	result += printBallance();

	return result;
}

std::string BankAccount::printAll()
{
	// Create a method called printAll that prints all the client details and the account ballance
	std::string result = "";
	result = this->owner->print();
	result += this->printBallance();

	return result;
}

std::string BankAccount::printBallance()
{
	std::string result = "";
	result = " Balance: ";
	result += std::to_string(this->balance);
	return result;
}