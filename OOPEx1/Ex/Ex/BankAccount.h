#pragma once
#include "Person.h"

#include <string>
//define a class called BankAccount. Implement all methods and test them.

// Each bank account has an owner of type Person pointer and a balance.
// Create parametrised constructor that takes a Person pointer as an argument and also sets the balance to 0
// Create a method called getBalance that returns the current value of the balance
// Create a method called deposit that takes an ammount of money and adds it to the balance
// Create a method called withdraw that takes an ammoun of money and takes it out of the balance
// Create a method called print that pretty prints the clients name and account ballance
// Create a method called printAll that prints all the client details and the account ballance

class BankAccount
{
public:
	BankAccount(Person* newOwner);
	float getBalance();
	void deposit(float ammount);
	bool withdraw(float ammount);
	std::string print();
	std::string printAll();
	
private:
	Person* owner;
	float balance;

	std::string printBallance();
};