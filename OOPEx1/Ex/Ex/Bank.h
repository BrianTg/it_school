#pragma once

#include "BankAccount.h"
//define a class called Bank. Implement all methods and test them.
// Each bank has an array of bank accounts that can hold up to 100 accounts

// Create a default constructor that sets the current number of clients to 0
// Create a method called addClient, that takes a Person pointer as an input and adds it to the array
// Create a method called removeClient, that removes a Person pointer from the array;
// Create a method called taxClients that takes a fixed amount and withdraws it from every clients balance
// Create a method called getTotalBalance that takes a Person pointer, and returns the cumulative balance of all of that persons accounts

class Bank
{
public:

private: 
	BankAccount clientAccounts[100];
	int clinetCount;
};