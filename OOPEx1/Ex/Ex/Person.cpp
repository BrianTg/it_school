#include"Person.h"

Person::Person()
{
	//sets its name and address to "unknown" and birt year to 0
	this->name = "unknown";
	this->address = "unknown";
	this->birthYear = 0;
}

Person::Person(const std::string& newName, const std::string& newAddress, int newBirthYear)
{
	this->name = newName;
	this->address = newAddress;
	this->birthYear = newBirthYear;
}

std::string Person::print()
{
	std::string result;
	//result  =""
	result = "Name: ";
	//result  ="Name: "
	result = result + this->name;
	//result  ="Name: [Person name]"
	result += " Address: ";
	result += this->address;
	result += " Birth year: ";
	result += std::to_string(this->birthYear);
	return result;
}

std::string Person::getName()
{
	return this->name;
}