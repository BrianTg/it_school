
#include "Employees.h"

#include <iostream>

std::string levelToString(Level level)
{
    std::string outputString;

    switch (level)
    {
    case JUNIOR:
        outputString = "Junior";
        break;
    case MID:
        outputString = "Mid";
        break;
    case SENIOR:
        outputString = "Senior";
        break;
    case EXPERT:
        outputString = "Expert";
        break;
    default:
        outputString = "unklown";
        break;
    }

    return outputString;
}

void printEmployee(Employee employee)
{
    std::cout << "Name: " << employee.name
        << "    Level:" << levelToString(employee.level)
        << "    Years: " << employee.yearsWithCompany
        << "    Salary: " << employee.salary
        << std::endl;
}

int main()
{
    
    //=================================================================================
    // 1 Print all employees
    //=================================================================================
    std::cout << "All the employees of the company:" << std::endl;

    for (int i = 0; i < nrOfEmployees; i++)
    {
        printEmployee(employees[i]);       
    }

    std::cout << std::endl;
    std::cout << std::endl;

    //=================================================================================
    // 2 Print the name and salary of the highest paid employee
    //=================================================================================
    std::cout << "The highest paid employee is: ";
 
    float max = employees[0].salary;
    Employee* highestPaidEmployee = employees;


    for (int i = 0; i < nrOfEmployees; i++)
    {
        if (employees[i].salary > max)
        {
            max = employees[i].salary;
            highestPaidEmployee = &employees[i];
        }
    }

    std::cout << highestPaidEmployee->name;
    
    std::cout << std::endl;
    std::cout << std::endl;

    //=================================================================================
    // 3 Print the name of the latest person to join the company
    //=================================================================================

    std::cout << "The latest employee is: ";

    // Print here

    std::cout << std::endl;
    std::cout << std::endl;

    //=================================================================================
    // 4 Print all the details of the Mid employees
    //=================================================================================

    std::cout << "The Mid employees are: ";

    // Print here

    std::cout << std::endl;
    std::cout << std::endl;


    //=================================================================================
    // 5 Calculate and print the average salary of an employee
    //=================================================================================

    std::cout << "The average salary is: ";

    // Print here

    std::cout << std::endl;
    std::cout << std::endl;

    //=================================================================================
    // 6 Calculate and print the average salary of a Mid level emplyee
    //=================================================================================

    std::cout << "The average Mid level salary is: ";

    // Print here

    std::cout << std::endl;
    std::cout << std::endl;

    //=================================================================================
    // 7 Vincent is joining the company as a Senior with a Salary of 2780.7
    //      Add him to the database (array) and print the new list of all the employees.
    //=================================================================================

    std::cout << "The new list of all employees is: ";

    // Print here

    std::cout << std::endl;
    std::cout << std::endl;
}
