#pragma once

#include <string>

#define MAX_NR_OF_EMPLOYEES 10

enum Level
{
	JUNIOR,
	MID,
	SENIOR,
	EXPERT
};

struct Employee
{
	std::string name;
	Level level;
	int yearsWithCompany;
	float salary;
};

extern Employee employees[MAX_NR_OF_EMPLOYEES];
extern int nrOfEmployees;
