#include "Employees.h"


int nrOfEmployees = 8;

Employee employees[MAX_NR_OF_EMPLOYEES] =
{//		name		level	years	salary
	{	"John",		MID,	2,		1520.7},
	{	"Sally",	MID,	3,		1730.0},
	{	"Jim",		SENIOR,	6,		2520.7},
	{	"Gill",		JUNIOR,	0,		1028.4},
	{	"Megan",	EXPERT,	3,		3028.4},
	{	"Ronald",	MID,	1,		1610.1},
	{	"Daniel",	JUNIOR,	1,		1206.0},
	{	"Ann",		MID,	1,		1606.0}
};