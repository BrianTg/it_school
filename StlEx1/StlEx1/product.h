#pragma once
#include <string>

enum ProductType
{
	VEGETABLE,
	DAIRY,
	MEAT
};

struct Product
{
	std::string name;		// Product name
	ProductType type;		// Product type
	float		price;		// Price per piece /  price per kilo
	float		quantity;   // Quantity
};