#include <iostream>
#include <vector>

#include "product.h"

std::vector<Product> groceries = 
{//		name			type			price		quantity	
	{	"Butter ",		DAIRY,			6.99,		1				},
	{	"Steak ",		MEAT,			24.6,		0.8				},
	{	"Carrots ",		VEGETABLE,		3.5,		1.5				},
	{	"Potatoes ",	VEGETABLE,		1.99,		2,				},
	{	"Milk ",		DAIRY,			2.5,		2				},
	{	"Onions ",		VEGETABLE,		2.99,		0.5				},
	{	"Sausage ",		MEAT,			18.0,		2				},
	{	"Garlic ",		VEGETABLE,		20.5,		0.1				},
	{	"Tomatoes ",	VEGETABLE,		3.5,		1.5				}
};

// write a function that takes a reference to a vector of products and returns the total cost of the products in the vector

float getTotalCost(const std::vector<Product>& inputVector)
{
	float totalCost = 0;

	for (const Product& currentProduct : inputVector)
	{
		totalCost += currentProduct.price * currentProduct.quantity;
	}

	return totalCost;
}

int main()
{
	// print every product's name in the groceries vector using an iterator

	std::cout << "The products in order are:" << std::endl;
	std::vector<Product>::iterator it;
	for (it = groceries.begin(); it != groceries.end(); ++it)
	{
		Product& currentProduct = *it;

		std::cout<< currentProduct.name << std::endl;
	}

	// print every product in the groceries vector using an iterator in reverse order
	std::cout <<std::endl<< "The products in reverse order are:" << std::endl;
	std::vector<Product>::reverse_iterator rit;

	for (rit = groceries.rbegin(); rit != groceries.rend(); ++rit)
	{
		Product& currentProduct = *rit;

		std::cout << currentProduct.name << std::endl;
	}

	// add a new product to the vector: Yoghurt dairy, 0.98, 6

	Product newProduct;

	newProduct.name = "Yoghurt";
	newProduct.price = 0.98;
	newProduct.type = DAIRY;
	newProduct.quantity = 6;

	groceries.push_back(newProduct);

	std::cout << std::endl<< "The products in order are:" << std::endl;

	for (it = groceries.begin(); it != groceries.end(); ++it)
	{
		Product& currentProduct = *it;

		std::cout << currentProduct.name << std::endl;
	}

	// remove yoghurt from the vector

	groceries.pop_back();

	std::cout << std::endl << "The products in order are:" << std::endl;

	for (it = groceries.begin(); it != groceries.end(); ++it)
	{
		Product& currentProduct = *it;

		std::cout << currentProduct.name << std::endl;
	}

	// add yoghurt to the vector in the 3rd position
	// remove Onions from the vector

	// print the combination of name and price for every product using a range for

	std::cout << std::endl << "The products in order are:" << std::endl;
	for (const auto& currentProduct : groceries)
	{
		std::cout << currentProduct.name << " " << currentProduct.price << std::endl;
	}

	std::cout << std::endl << "The total price of the griceries is :" << getTotalCost(groceries) << std::endl;

	// write a function that takes a reference to a vector of products and an amount of money and prints all products cheaper than the ammount
	
	// create vector of pointers to products and populate it with the pointer to vegetables and print it

	// write a find function that takes a string as an input, and a float as output and returns a boolean
		// if a product is found by name place its price in the output argument and return true
		// if a product is not found by name return false
}


