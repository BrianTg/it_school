#pragma once

#include<string>

struct Car
{
	std::string chasisSeries;
	std::string model;
	int year;
};
