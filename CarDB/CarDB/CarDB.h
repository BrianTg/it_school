#pragma once

#include "Car.h"
#include <map>

class CarDB
{
	std::map<std::string, Car> cars;
public:


	void addCar(std::string chSeries, std::string model, int year);	
	void printAllCars();
	bool findCarModel(std::string chSeries, std::string* model);/*
	void printCarsNewerThan(int year);
	void remove(std::string chSeries);
	int	 getDbSize();
	void printCarsOfModel(std::string model);
	*/
private:
	void printCar(const Car* car);
};