#include <iostream>
#include "CarDB.h"


int main()
{
	//Car dacie("DA56gy", "Solenza", 1994);
	//Car jetta("WV76fr", "Jetta", 2011);

	CarDB db;

	db.addCar("DA56gy", "Solenza", 1994);
	db.addCar("WV76fr", "Jetta", 2011);
	

	db.printAllCars();

	std::string result;
	if (db.findCarModel("WV76fr", &result))
	{
		std::cout << "We found a car, the model is " << result << std::endl;
	}
	else
	{
		std::cout << "We didn't find a car " << std::endl;
	}
}