#include "CarDB.h"
#include <iostream>

void CarDB::addCar(std::string chSeries, std::string model, int year)
{
	std::pair<std::string, Car> newCarEntry;

	newCarEntry.first = chSeries;

	newCarEntry.second.chasisSeries = chSeries;
	newCarEntry.second.model = model;
	newCarEntry.second.year = year;

	//Car& newcar = newCarEntry.second;
	//newcar.model = model;

	this->cars.insert(newCarEntry);
}

void CarDB::printCar(const Car* car)
{
	std::cout << "Series: " << car->chasisSeries << " Model: " << car->model << " year: " << car->year << std::endl;
}

void CarDB::printAllCars()
{
	for (auto& pair : this->cars)
	{
		Car* currentCar = &pair.second;
		printCar(currentCar);
	}
}

bool CarDB::findCarModel(std::string chSeries, std::string* model)
{
	std::map<std::string, Car>::iterator it;
	it = cars.find(chSeries);
	if (it != cars.end())
	{
		// it -> iterator
		// *it -> pair at "it"
		// (*it).second  -> the car we found
		// (*it).second.model -> the model of the car we found

		*model = (*it).second.model;
		return true;
	}
	else
	{
		return false;
	}
}


