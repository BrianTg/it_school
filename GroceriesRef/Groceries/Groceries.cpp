#include <iostream>
#include "Product.h"

int main()
{
    //=================================================================================
    // FOR all points, write the function in the appropriate module
    //      and test it in this file
    //=================================================================================

    //=================================================================================
    // 1 Change the printProduct function to use references, print the grocery list
    //=================================================================================
    
    //=================================================================================
    // 2 Write a find function.
    //      takes a string name as input 
    //      takes a pointer to a product index as an output
    //  returnes true if product name is found, false if it is not
    //
    //  Try to find Butter and Peppers
    //=================================================================================

    //=================================================================================
    // 3 Write a resetQuanitity function
    //      takes a reference of a product as input
    //
    //  Reset the quantity of Carrots
    //=================================================================================


    //=================================================================================
    // 4 Write a function that sets the VAT for a product
    //      the VAT should be a number between 0 an 1.
    //      If the value is out of bouds the function should return false.
    //
    //      Test by setting 0.23 to Milk and 2.7 to Butter
    //=================================================================================

    //=================================================================================
    // 5 Implement and use the following function
    // bool getPrice(const std::string& name, float& price);
    //     name- input parameter
    //     price - output parameter
    //     return bool - true if found, false if not found
    //
    //  Test getPrice for Potatoes and for Squash
    //=================================================================================


    //=================================================================================
    // 6 Write a function that takes 2 products 
    //      and returns a reference to the more expensive product
    //
    //  Test with shoppingList[0] and shoppingList[1]
    //=================================================================================

    
    //=================================================================================
    // 7 create a global array of pointers to Products name it Meat
    //      Create a function that populates the Meat array
    //      with pointers too the grocery list elements that are of type meat.
    //
    //  Test by printing the elements from MEAT
    //=================================================================================
    
}