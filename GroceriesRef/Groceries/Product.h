#pragma once

#include <string>

// max size of shopping list
#define SHOPPING_LIST_SIZE 10

enum ProductType
{
	VEGETABLE,
	DAIRY,
	MEAT
};

struct Product
{
	std::string name;		// Product name
	ProductType type;		// Product type
	float		price;		// Price per piece /  price per kilo
	float		quantity;   // Quantity
	float		VAT;		// VAT Tax ( depends on product type) 0 => 0%  0.5 => 50% 1 => 100% 
};

extern int shoppingListSize;
extern Product shoppingList[SHOPPING_LIST_SIZE];



void printProduct(Product* product);


// Function that calculates the price of a product and returns it
//		Price =  Price without VAT + Tax
//		Price without VAT =  (Price per piece/kilo) * Quantity
//		Tax = (Price without VAT)* VAT
float calculateProductPrice(Product* product);
