#pragma once
class Wheel
{
public:
	Wheel();
	bool getSpinningState();
	void setSpinningState(bool newState);
private:
	bool isSpinning;
};
