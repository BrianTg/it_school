#pragma once

#include "Wheel.h"
#include <string>
class Car 
{
public:

	Car();
	~Car();

	int getSerialNumber();
	void start();
	void stop();
	std::string getDiagnostics();
private:
	static int nextAvailableSerialNumber;

	int serialNumber;
	bool isRunnig;
	Wheel* wheels;
};