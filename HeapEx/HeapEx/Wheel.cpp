#include "Wheel.h"

Wheel::Wheel()
{
	setSpinningState(false);
}

bool Wheel::getSpinningState()
{
	return this->isSpinning;
}

void Wheel::setSpinningState(bool newState)
{
	this->isSpinning = newState;
}
