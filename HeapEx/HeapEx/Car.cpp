#include "Car.h"

// for every exercise implement, then test;

// Allocate heap memory for the array of 4 wheels,
// Make sure the car has a unique serial number
// Make sure the car is stopped when created

int Car::nextAvailableSerialNumber = 0;

Car::Car()
{
	this->wheels = new Wheel[4];
	this->serialNumber = this->nextAvailableSerialNumber++;
	this->isRunnig = false;
}

// Deallocate heap memory for the array of 4 wheels
Car::~Car()
{
	delete[] this->wheels;
}

// returns the serial number of the car
int Car::getSerialNumber()
{
	return this->serialNumber;
}

// changes the state of the car and of the wheels
void Car::start()
{
	this->isRunnig = true;

	for (int i = 0; i < 4; i++)
	{
		this->wheels[i].setSpinningState(true);
	}
}

// changes the state of the car and of the wheels
void Car::stop()
{
	this->isRunnig = false;

	for (int i = 0; i < 4; i++)
	{
		this->wheels[i].setSpinningState(false);
	}
}

// returns a pretty string containing the cars' serial number, it's state (running or stopped) and the state of all the wheels
std::string Car::getDiagnostics()
{
	std::string result = "";

	result += "Serial ";
	result += std::to_string(this->serialNumber);
	result += " State is ";

	if (this->isRunnig)
	{
		result += " Runnig ";
	}
	else
	{
		result += " Stopped ";
	}

	for (int i = 0; i < 4; i++)
	{
		result += " wheel ";
		result += std::to_string(i);
		result += " is ";

		if (this->wheels[i].getSpinningState())
		{
			result += " Spinning ";
		}
		else
		{
			result += " Stopped ";
		}
	}

	return result;
}