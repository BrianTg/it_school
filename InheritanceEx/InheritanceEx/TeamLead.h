#pragma once

#include "Employee.h"

#define MAX_TEAM 10

class TeamLead : public Employee
{
public:
    TeamLead();
    ~TeamLead();
    TeamLead(const std::string& newName, const std::string& newEmail, float newSalary);
    bool addTeamMember(Employee* newMember);
    float getAverageTeamSalary();

private:
    Employee* teamMembers;
    int teamMemberSize;
};