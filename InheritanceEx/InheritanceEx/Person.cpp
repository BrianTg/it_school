#pragma once

#include "Person.h"
Person::Person() : Person("","") {}
Person::Person(const std::string& newName, const std::string& newEmail)
{
	this->name = newName;
	this->email = newEmail;
}

std::string Person::getName()
{
	return this->name;
}
std::string Person::getAddress()
{
	return this->email;
}