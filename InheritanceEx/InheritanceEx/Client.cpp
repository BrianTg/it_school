#include "Client.h"



Client::Client()
{
	this->clientId = 0;
}


Client::Client(const std::string& newName, const std::string& newEmail, int newClientId) 
	: Person(newName, newEmail)
{
	this->clientId = newClientId;
}


int Client::getClientId()
{
	return this->clientId;
}

std::string Client::getAddress()
{
	return "EXTERNAL! ";
}