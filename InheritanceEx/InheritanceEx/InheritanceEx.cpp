#include <iostream>
#include "Person.h"
#include "Client.h"
#include "Employee.h"

int main()
{

   //implement and test Person

    Person* p1 = new Person("Larry", "larry@gmail.com");

    std::cout << p1->getName() << " " << p1->getAddress() << std::endl;


   //implement and test Client

    Client* c1 = new Client("Jenny", "jenny@gmail.com", 765);

    std::cout << c1->getName() << " " << c1->getAddress() << std::endl;

    delete p1, c1;
       /*
   implement and test Employee
   implement and test TeamLead
         name  Larry
         email larry@gmail.com
         salary 2800.0
         teamMembers:
             name  Jenny
             email jenny@gmail.com
             salary 1200.7

             name  Michael
             email michael@gmail.com
             salary 1690.6

             name  Ann
             email ann@gmail.com
             salary 1790.2
            
*/
 
}