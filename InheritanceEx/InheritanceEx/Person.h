#pragma once

#include <string>

class Person
{
public:
    Person();
    Person(const std::string& newName, const std::string& newEmail);
    std::string getName();
    std::string getAddress();
private:
    std::string name;
    std::string email;
};
