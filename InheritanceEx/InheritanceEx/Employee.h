#pragma once

#include "Person.h"

class Employee : public Person
{
public:
    Employee();
    Employee(const std::string& newName, const std::string& newEmail, float newSalary);
    Employee(const Person& person);
    void setSalary(float newSalary);
    float getSalary();
private:
    float salary;
};
