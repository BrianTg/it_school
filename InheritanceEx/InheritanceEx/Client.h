#include "Person.h"

class Client : public Person
{
public:
    Client();
    Client(const std::string& newName, const std::string& newEmail, int newClientId);
    int getClientId();
    std::string getAddress();
private:
    int clientId;
};