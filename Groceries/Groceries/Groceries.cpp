#include <iostream>
#include "Product.h"

int main()
{
   //=================================================================================
   // 1 Print all produce on the grocery list (shoppingList)
   //   use printProduct (already implemented)
   //=================================================================================
    std::cout << "All the produce:" << std::endl;

    // Print all produce here
    for (int i = 0; i < shoppingListSize ; i++)
    {
        printProduct(&shoppingList[i]);
    }

    std::cout << std::endl;
    std::cout << std::endl;

    //=================================================================================
    // 2 Find the most expensive product per unit from the list and print it
    //=================================================================================
    std::cout << "The most expensive product is:";

    // Print all products here

    std::cout << std::endl;
    std::cout << std::endl;

    //=================================================================================
    // 3 Implement the calculateProductPrice (shoppingList.cpp)
    //  calculate the price of the first item on the list
    //=================================================================================
    std::cout << "The price of the first product on the list is : ";

    // Print price here

    std::cout << std::endl;
    std::cout << std::endl;

    //=================================================================================
    // 4 Calculate and print the total price of the grocery list ( sum of all produce )
    //=================================================================================

    std::cout << "The total price of the grocery list is: ";

    // Print here

    std::cout << std::endl;
    std::cout << std::endl;

    //=================================================================================
    // 5 Change the VAT of all dailry products in the list to 12%.
    // Calculate the new total of the grocery list
    //=================================================================================

    std::cout << "The new list total is: ";

    // Print here

    std::cout << std::endl;
    std::cout << std::endl;


    //=================================================================================
    // 6 I am turning vegetarian. Remove all Meat from the grocery list. 
    // Print the new List
    //=================================================================================

    std::cout << "The new list is: ";

    // Print here

    std::cout << std::endl;
    std::cout << std::endl;

}
