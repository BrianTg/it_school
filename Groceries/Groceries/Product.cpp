#include "Product.h"

#include <string>
#include <iostream>

std::string productTypeToString(ProductType type)
{
	std::string output;
	switch (type)
	{
	case VEGETABLE:
		output = "Vegetable";
		break;
	case DAIRY:
		output = "Dairy";
		break;
	case MEAT:
		output = "Meat";
		break;
	default:
		output = "Unkown";
		break;
	}
	return output;
}

void printProduct(Product* product)
{
	std::cout << "Name: " << product->name
		<< "		Type: " << productTypeToString(product->type)
		<< "		PricePerUnit: " << product->price
		<< "	Quantity: " << product->quantity
		<< "	VAT: " << product->VAT * 100 << "%" << std::endl;
}


// Function that calculates the price of a product and returns it
//		Price =  Price without VAT + Tax
//		Price without VAT =  (Price per piece/kilo) * Quantity
//		Tax = (Price without VAT)* VAT
float calculateProductPrice(Product* product)
{
	// Implement this here
	return 0.0;
}