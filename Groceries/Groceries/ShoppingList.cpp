#include "Product.h"


int shoppingListSize = 9;

Product shoppingList[SHOPPING_LIST_SIZE]
{//		name			type			price		quantity	VAT
	{	"Butter ",		DAIRY,			6.99,		1,			0.24	},
	{	"Steak ",		MEAT,			24.6,		0.8,		0.3		},
	{	"Carrots ",		VEGETABLE,		3.5,		1.5,		0.19	},
	{	"Potatoes ",	VEGETABLE,		1.99,		2,			0.19	},
	{	"Milk ",		DAIRY,			2.5,		2,			0.24	},
	{	"Onions ",		VEGETABLE,		2.99,		0.5,		0.19	},
	{	"Sausage ",		MEAT,			18.0,		2,			0.3		},
	{	"Garlic ",		VEGETABLE,		20.5,		0.1,		0.19	},
	{	"Tomatoes ",	VEGETABLE,		3.5,		1.5,		0.19	}
};
