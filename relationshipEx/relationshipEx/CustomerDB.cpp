#include "CustomerDB.h"

Customer* CustomerDB::getCustomer(std::string customerName)
{
	std::vector<Customer>::iterator it;

	for (it = this->customers.begin(); it != this->customers.end(); ++it)
	{
		if ((*it).getName() == customerName)
		{
			return &(*it);
		}
	}

	return nullptr;
}

bool CustomerDB::addCustomer(std::string newName)
{
	if(nullptr == getCustomer(newName))
	{
		Customer newCustomer(newName);
		this->customers.push_back(newCustomer);
		return true;
	}

	return false;
}

bool CustomerDB::addCustomer(Customer* newCustomer)
{

	if (nullptr == getCustomer(newCustomer->getName()))
	{
		this->customers.push_back(*newCustomer);
		return true;
	}

	return false;
}