#include "Book.h"

Book::Book() : Book("", "") {}

Book::Book(std::string newTitle, std::string newAuthor)
{
	this->title = newTitle;
	this->author = newAuthor;
}

void Book::addPage(std::string newText)
{
	Page newPage(newText);
	pages.push_back(newPage);
}

std::string Book::getContent()
{
	std::string bookText = "";
	
	std::vector<Page>::iterator	it;

	for (it = pages.begin(); it != pages.end(); ++it)
	{
		bookText += (*it).getText();
		bookText += '\n';
	}
	return bookText;
}

std::string Book::getTitle()
{
	return this->title;
}
std::string Book::getAuthor()
{
	return this->author;
}