#pragma once

#include"Page.h"

#include <vector>
#include <string>

class Book
{
public:

	Book();
	Book(std::string newTitle, std::string newAuthor);

	void addPage(std::string newText);
	std::string getContent();

	std::string getTitle();
	std::string getAuthor();
private:
	std::vector<Page> pages;
	std::string author;
	std::string title;
};