#include <iostream>
#include <vector>

#include "Book.h"
#include "Customer.h"
#include "CustomerDB.h"

int main()
{
    Page p("my text that is soooo beautiful \n");
    Page q(p);

    std::cout << q.getText() << std::endl;

    Page finalPage("This is the nice ending");

    Book myBook("The greatest book", "me");
    std::cout << myBook.getTitle() << std::endl;
    std::cout << myBook.getAuthor() << std::endl;

    myBook.addPage(" Intro \n");
    myBook.addPage(" chapter 1 \n");
    myBook.addPage(" chapter 2 \n");
    myBook.addPage(" chapter 3 \n");
    myBook.addPage(" chapter 4 \n");
    myBook.addPage(" Ending \n");

    std::cout << myBook.getContent() << std::endl;




    Customer cust("George");
    std::cout << cust.getName() << std::endl;


    Book* rentedBook= nullptr;
    if (cust.getRentedBook(&rentedBook)==true)
    {
        std::cout << rentedBook->getContent() << std::endl;
    }
    else
    {
        std::cout << "Customer has nor rented a book" << std::endl;
    }

    cust.rentBook(&myBook);

    if (cust.getRentedBook(&rentedBook) == true)
    {
        std::cout << rentedBook->getContent() << std::endl;
    }
    else
    {
        std::cout << "Customer has nor rented a book" << std::endl;
    }






    CustomerDB db;

    db.addCustomer("Michael");

    Customer* custPtr = db.getCustomer("Michael");

    if (custPtr != nullptr)
    {
        std::cout << custPtr->getName() << std:: endl;
    }
    else
    {
        std::cout << "Customer not found" << std::endl;
    }

    db.addCustomer(&cust);

     custPtr = db.getCustomer("George");

    if (custPtr != nullptr)
    {
        std::cout << custPtr->getName() << std::endl;
    }
    else
    {
        std::cout << "Customer not found" << std::endl;
    }


}