#include <iostream>

void showMenu()
{
	std::cout << "=====================================================================" << std::endl;
	std::cout << "		1 - Add customer" << std::endl;
	std::cout << "		2 - Remove customer" << std::endl;
	std::cout << "		3 - Search for customer" << std::endl;
	std::cout << "		4 - Rent book" << std::endl;
	std::cout << "		5 - Return book" << std::endl;
	std::cout << "		0 - Exit" << std::endl;
	std::cout << "=====================================================================" << std::endl;
}

void clearScreen()
{
	system("cls");
}