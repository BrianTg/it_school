#pragma once

#include "Book.h"

#include <string>


class Customer 
{
public:
	Customer();
	Customer(std::string newName);

	std::string getName();
	bool getRentedBook(Book** customerRentedBook);

	void rentBook(Book* customerRentedBook);
	void returnBook();


private:
	std::string name;
	bool hasRentedBook;
	Book* rentedBook;
};