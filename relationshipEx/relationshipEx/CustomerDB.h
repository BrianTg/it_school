#pragma once

#include "Customer.h"

#include<vector>

class CustomerDB 
{
public:

	Customer* getCustomer(std::string customerName);

	bool addCustomer(std::string newName);
	bool addCustomer( Customer* newCustomer);
	

/*
	
	getCustomerNumber()
	getNrOfRentedBooks()

	removeCustomer()
*/

private:
	std::vector<Customer> customers;
};