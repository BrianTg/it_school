#pragma once
#include <string>

class Page 
{
public:
	Page();
	Page(std::string newText);

	std::string getText();
private:
	std::string text;
};