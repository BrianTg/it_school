#include "Customer.h"

Customer::Customer() : Customer("Unused") {}

Customer::Customer(std::string newName)
{
	this->name = newName;
	this->hasRentedBook = false;
	this->rentedBook = nullptr;
}

std::string Customer::getName()
{
	return this->name;
}

bool Customer::getRentedBook(Book** customerRentedBook)
{
	*customerRentedBook = this->rentedBook;
	return this->hasRentedBook;
}

void Customer::rentBook(Book* customerRentedBook)
{
	this->rentedBook = customerRentedBook;
	this->hasRentedBook = true;
}

void Customer::returnBook()
{
	this->rentedBook = nullptr;
	this->hasRentedBook = false;
}