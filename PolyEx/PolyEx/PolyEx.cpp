
#include <iostream>
#include "AbsClasses.h"
#include "Bus.h"
#include "Car.h"
#include "Pickup.h"
#include "Semitruck.h"


// write a method that prints entity names
// write a method that starts vehicles
// write a method that stops vehicles
// an array of cargo carriers, that we want to load and unload
// an array of vehicles on wich to call the start function for each and then the stop function for each
// create a class called loading bay that can load and unload cargo carriers
// create a class called Barracks that can load the ammo to weapons
// create a method that is called testRange that shoots weapons
// create an array of wapons and test an instance of the barracs and the testRange method


void print(Entity& entity)
{
    std::cout << entity.getName() << std::endl;
}

void startVehicle(Vehicle* vehicle)
{
    vehicle->drive();
}

int main()
{
    Bus mySchoolBus, prisonBus;
    Car ferrari, dacie;
    Pickup papuc;
    SemiTruck volvo;

    print(mySchoolBus);
    print(papuc);

    startVehicle(&papuc);
    startVehicle(&ferrari);
}
