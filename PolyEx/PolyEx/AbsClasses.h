#pragma once
#include <string>
class Entity 
{
public:
	virtual std::string getName() = 0;
};

class CargoCarrier 
{
protected:
	bool isFullOfCargo;
public:
	virtual void load() = 0;
	virtual void unload() = 0;
};

class PeopleCarrier
{
protected:
	bool isFullOfPeople;
public:
	virtual void board() = 0;
	virtual void unboard() = 0;
};

class Vehicle
{
protected:
	bool isRunning;
public:
	virtual void drive() = 0;
	virtual void stop() = 0;
};

class Weapon
{
protected:
	bool hasAmmo;
public:
	virtual void loadAmmo() = 0;
	virtual void shoot() = 0;
};
