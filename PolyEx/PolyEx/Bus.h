#pragma once
#include "AbsClasses.h"
#include <iostream>

class Bus : public Vehicle, public Entity, public PeopleCarrier
{
	void drive()
	{
		this->isRunning = true;
		std::cout << "Driving with anger" << std::endl;
	}

	void stop()
	{
		this->isRunning = false;
		std::cout << "Stopping in a station" << std::endl;
	}

	std::string getName()
	{
		return "Bus";
	}

	void board()
	{
		this->isFullOfPeople = true;
		std::cout << "Boarding the passengers one by one" << std::endl;
	}

	void unboard()
	{
		this->isFullOfPeople = false;
		std::cout << "Unboarding the passengers one by one" << std::endl;
	}

};
