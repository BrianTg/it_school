#pragma once
#include "AbsClasses.h"
#include <iostream>

class SemiTruck : public CargoCarrier, public Vehicle, public Entity
{
	void load() override
	{
		this->isFullOfCargo = true;
		std::cout << "Loading with a forklift" << std::endl;
	}

	void unload()
	{
		this->isFullOfCargo = false;
		std::cout << "Unloading with a forklift" << std::endl;
	}


	void drive()
	{
		this->isRunning = true;
		std::cout << "Driving with 14 gears" << std::endl;
	}

	void stop()
	{
		this->isRunning = false;
		std::cout << "Stopping with the servoBreak" << std::endl;
	}

	std::string getName()
	{
		return "SemiTruck";
	}

};
