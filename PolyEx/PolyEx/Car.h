#pragma once
#include "AbsClasses.h"
#include <iostream>

class Car : public Vehicle, public Entity, public PeopleCarrier
{
	void drive()
	{
		this->isRunning = true;
		std::cout << "Driving with style" << std::endl;
	}

	void stop()
	{
		this->isRunning = false;
		std::cout << "Stopping easily" << std::endl;
	}

	std::string getName()
	{
		return "Car";
	}

	void board()
	{
		this->isFullOfPeople = true;
		std::cout << "Boarding as we wish" << std::endl;
	}

	void unboard()
	{
		this->isFullOfPeople = false;
		std::cout << "Unboarding as we wish" << std::endl;
	}

};
