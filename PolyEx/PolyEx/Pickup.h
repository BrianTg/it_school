#pragma once
#include "AbsClasses.h"
#include <iostream>

class Pickup : public CargoCarrier, public Vehicle, public Entity, public PeopleCarrier
{
	void load() override
	{
		this->isFullOfCargo = true;
		std::cout << "Loading by hand" << std::endl;
	}

	void unload()
	{
		this->isFullOfCargo = false;
		std::cout << "Unloading by hand" << std::endl;
	}


	void drive()
	{
		this->isRunning = true;
		std::cout << "Driving with country music" << std::endl;
	}

	void stop()
	{
		this->isRunning = false;
		std::cout << "Stopping with handbreak" << std::endl;
	}

	std::string getName()
	{
		return "PickupTruck";
	}

	void board() 
	{
		this->isFullOfPeople = true;
		std::cout << "Boarding the pickuptruck" << std::endl;
	}

	void unboard()
	{
		this->isFullOfPeople = false;
		std::cout << "Unboarding the pickuptruck" << std::endl;
	}

};
