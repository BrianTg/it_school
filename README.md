# Set-up #
Follow these steps to set up your environment:
#### Pre-Conditions
You will need 7 GB free Space on ___C:___

#### Visual Studio ####
- Go to the [Visual Studio](https://docs.microsoft.com/en-us/visualstudio/install/install-visual-studio?view=vs-2019) installation page
- From Workloads choose ___Desktop development with C++___
- From the list of components Uncheck ___Test Adapter for Boost.Test___ and ___Live Share___
- Install
- Close when done

#### Git ####
- Download Git from [here](https://git-scm.com/download/win). Git is a tool we will use to get our source materials (for now)
- Run the Installer, click "Next" for all steps
- Test that it works 
  - Open Start Menu, type "powershell" click on PowerShell Icon
  - Type git , you should get 
  
    ```
    usage: git [--version] [--help] [-C <path>] [-c <name>=<value>]
    [--exec-path[=<path>]] [--html-path] [--man-path] [--info-path]
    [-p | --paginate | -P | --no-pager] [--no-replace-objects] [--bare]
    [--git-dir=<path>] [--work-tree=<path>] [--namespace=<name>]
     <command> [<args>]
    ```
 - If it doesn't work, add Git to the Environment Variable list
   - Open Start Menu, type "Environment Variables"
   - Click on Environment Variables
   - Click on "Path" form the list above
   - Edit
   - New, then type "C:\Program Files\Git\bin"
   - OK 3 times
   - Test Again
   
#### The Materials ####
- Create a new folder named "___IT_School___"  in ___C:\___
- From PowerShell type
	
    ```
   cd C:\IT_School\
    ```
	
    ```
    git clone https://BrianTg@bitbucket.org/BrianTg/it_school.git
    ```
	
    ```
    .\it_school_dev\EnvironentSetup.bat
    ```
	
- Close PowerShell

#### We are ready to go! ####
